﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Üyelik.aspx.cs" Inherits="aracsatis.Üyelik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 953px;
        }
        .auto-style2 {
            width: 348px;
            text-align: center;
        }
        .auto-style3 {
            width: 945px;
        }
        .auto-style4 {
            width: 309px;
        }
        .auto-style5 {
            width: 93px;
        }
        .auto-style7 {
            width: 122px;
        }
        .auto-style8 {
            width: 369px;
        }
        .auto-style9 {
            width: 94px;
        }
        .auto-style10 {
            width: 409px;
        }
        .auto-style11 {
            width: 1053px;
        }
        .auto-style13 {
            width: 220px;
            height: 67px;
        }
        .auto-style14 {
            width: 1053px;
            height: 67px;
        }
        .auto-style15 {
            width: 220px;
        }
    </style>
</head>
<body>
    
    <form id="form1" runat="server" style="background-color:aqua">
    
        <table style="height: 385px">
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style2">Yeni Üyelik</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style2">
                    <table>
                        <tr>
                            <td class="auto-style5">Ad :</td>
                            <td class="auto-style4">
                                <asp:TextBox ID="yeniad" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style2">
                    <table>
                        <tr>
                            <td class="auto-style7">Şifre :</td>
                            <td class="auto-style10">
                                <asp:TextBox ID="yenisifre" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style2">
                    <table>
                        <tr>
                            <td class="auto-style9">Email :</td>
                            <td class="auto-style8">
                                <asp:TextBox ID="yenimail" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style2">&nbsp;&nbsp;
                    <asp:Button ID="uyeol" runat="server" Text="Üye Ol !!" OnClick="uyeol_Click" />
                </td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
        </table>
    
    </form>
    <table>
        <tr>
            <td class="auto-style13"></td>
            <td class="auto-style14">
                <p style="border-style: none; border-color: inherit; border-width: 0px; margin: 0px; padding: 0px; outline: 0px; font-size: 14px; text-decoration: none; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22.399999618530273px; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
                    Üye Olduktan Sonra Bir Önceki Sayfamıza Giderek ADINIZ ve ŞİFRE &#39;nizle Giriş Yapa Bilirsiniz</p>
                <p style="border-style: none; border-color: inherit; border-width: 0px; margin: 0px; padding: 0px; outline: 0px; font-size: 14px; text-decoration: none; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22.399999618530273px; orphans: auto; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
                    Kayıt Olduğunuz İçin Teşekkür Ederiz</p>
                <p style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; text-decoration: none; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 22.399999618530273px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);">
                    &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="auto-style15">&nbsp;</td>
            <td class="auto-style11">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style15">&nbsp;</td>
            <td class="auto-style11">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style15">&nbsp;</td>
            <td class="auto-style11">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style15">&nbsp;</td>
            <td class="auto-style11">&nbsp;</td>
        </tr>
    </table>
</body>
</html>
